set -euo pipefail
IFS=$'\n\t'

##
#  A helper function to check if one or more files with the given extension exist
#  ------------------------------------------------------------------------------
have_ext() {
  ext="$1"
  rcode=0
  ls *."$ext" &> /dev/null || rcode=1
  return $rcode
}
