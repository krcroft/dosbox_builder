set -euo pipefail
IFS=$'\n\t'

##
#  Rebuild with CMake
#  ------------------
cbuild() {
  cd "$dir"
  CFLAGS="$flags" CXXFLAGS="$flags" LDFLAGS="$flags" cmake "$@" &> "$log"
  make -j"$num_procs" &> "$log"
  ran=True
}
