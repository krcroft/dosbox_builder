set -euo pipefail
IFS=$'\n\t'
ran=False
##
#  Reconfigure the package if updates were pulled
#  ----------------------------------------------
if [[ -z "$dir" ]]; then
  return 0
fi

cd "$dir"

if [[ -n "$patch" ]]; then
  $patch &> "$log" || return 1
  ran=True
elif [[ -n "$patches" ]]; then
  for patch in "${patches[@]}"; do
    ( curl "${curl_opts[@]}" "$patch" | patch -p1 ) &> "$log"  || return 1
  done
  ran=True
fi

if [[ -n "$post_patch" ]]; then
  $post_patch &> "$log" || return 1
  ran=True
fi
