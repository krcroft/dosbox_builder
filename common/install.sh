set -euo pipefail
IFS=$'\n\t'
ran=False

##
#  Reinstall the package if updates were pulled
#  --------------------------------------------

if [[ -z "$dir" ]]; then
  return 0
fi

cd "$dir"
if [[ -n "$install" ]]; then
  $install &> "$log" || return 1
  ran=True
else
  eval "${install_env[@]}" make "${install_flags[@]}" install &> "$log" || return 1
  ran=True
fi

# Confirm expected outputs have been created
if [[ -n "$creates" ]]; then
  for expected in "${creates[@]}"; do
    if [[ ! -f "$prefix/$expected" ]]; then
      echo "$package did not install expected file $prefix/$expected"
      return 1
    fi
  done
fi

if [[ -n "$post_install" ]]; then
  $post_install &> "$log" || return 1
  ran=True
fi

