set -euo pipefail
IFS=$'\n\t'

##
#  Clone and update or Fetch a package
#  -----------------------------------

if [[ -n "$pull" ]]; then
  $pull &> "$log" || return 1
  ran=True
else
  # no action needed ..
  if [[ -z "$url" || -z "$dir" ]]; then
    return 0
  fi

  if [[ ! -d "$dir" ]]; then
    if [[    "$url" == *"tar.gz"* ]]; then
      mkdir -p "$dir"
      ( curl "${curl_opts[@]}" "$url" | tar -z -x --strip-components=1 -C "$dir" ) &> "$log" || return 1
    elif [[ "$url" == *"tar.xz"* ]]; then
      mkdir -p "$dir"
      ( curl "${curl_opts[@]}" "$url" | tar -J -x --strip-components=1 -C "$dir" ) &> "$log" || return 1
    elif [[ "$url" == *"tar.bz2"* ]]; then
      mkdir -p "$dir"
      ( curl "${curl_opts[@]}" "$url" | tar -j -x --strip-components=1 -C "$dir" ) &> "$log" || return 1
    else
      [[ -n "$branch" ]] || branch=master
      git clone --depth 1 -b "$branch" "$url" "$dir" &> "$log" || return 1
    fi || rm -rf "$dir"
  fi

  if [[ -d "$dir/.git" ]]; then
    cd "$dir"
    git pull &> "$log" || return 1
  fi
  ran=True
fi

if [[ -n "$post_pull" ]]; then
  $post_pull &> "$log" || return 1
  ran=True
fi
