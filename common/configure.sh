set -euo pipefail
IFS=$'\n\t'
ran=False

##
#  Reconfigure the package if updates were pulled
#  ----------------------------------------------
if [[ -z "$dir" ]]; then
  return 0
fi

mkdir -p "$prefix/include" "$prefix/lib" "$prefix/bin"

original_path="$PATH"
export PATH="$prefix/bin:$PATH"

cd "$dir"
if [[ -n "$configure" ]]; then
  $configure &> "$log" || return 1
  ran=True
else
  # Pre-configure if needed
  if [[ ! -x configure ]]; then
    if [[ -x autogen.sh ]]; then ./autogen.sh || return 1; fi
    if [[ -x bootstrap ]]; then ./bootstrap || return 1; fi
    if [[ ! -x configure ]]; then autoreconf -i || return 1; fi
  fi &> "$log"

  # Configure the package
  if [[ -x configure ]]; then

      CFLAGS="${flags[@]} ${cflags[@]}" \
      CXXFLAGS="${flags[@]} ${cxxflags[@]}" \
      CPPFLAGS="-I$prefix/include ${cppflags[@]}" \
      LDFLAGS="${flags[@]} -L$prefix/lib ${ldflags[@]}" \
      LIBS="${libs[@]}" \
      LD_LIBRARY_PATH="$prefix" \
      ./configure --prefix="$prefix" "${configure_flags[@]}" &> "$log" || return 1
  fi
  ran=True
fi

if [[ -n "$post_configure" ]]; then
  $post_configure &> "$log" || return 1
  ran=True
fi

export PATH="$original_path"
