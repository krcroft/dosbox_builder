set -euo pipefail
IFS=$'\n\t'
ran=False

##
#  Remake the package if updates were pulled
#  -----------------------------------------
if [[ -z "$dir" ]]; then
  return 0
fi

original_path="$PATH"
export PATH="$prefix/bin:$PATH"

cd "$dir"

if [[ -n "$make" ]]; then
  $make &> "$log" || return 1
  ran=True

else
  # Build and install
  # make clean &> "$log" || return 1
  eval "${make_env[@]}" make -j"$num_procs" "${make_flags[@]}" &> "$log" || return 1
  ran=True
fi

if [[ -n "$post_make" ]]; then
  $post_make &> "$log" || return 1
  ran=True
fi

export PATH="$original_path"
