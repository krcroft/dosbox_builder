set -euo pipefail
IFS=$'\n\t'
ran=False
##
#  Install libraries, headers, and executables pre-built and shipped in a zip
#  --------------------------------------------------------------------------
#  Args:
#    - zip_url is the full URL to download a zip file.
#    - zip_dir is the name of the top-level directory that's contained in the zip file
#    - zip_header_subdir is the (optional) name of the $prefix/include/sub-directory
#      in which to install headers


if [[ -n "$zip_url" && -n "$zip_dir" ]]; then
  lib_dir="$prefix/lib"
  bin_dir="$prefix/bin"
  header_dir="$prefix/include"
  install_needed=False
  
  if [[ "$zip_dir" == "." ]]; then
    zip_dir=$(basename "$zip_url" .zip)
    if [[ ! -d "$zip_dir" ]]; then
	  install_needed=True
	  mkdir -p "$zip_dir"
	  (
	    set -euo pipefail
	    cd "$zip_dir"
        zip="$$.zip"
        curl "${curl_opts[@]}" --progress-bar "$zip_url" -o "$zip" &> "$log" || return 1
	    unzip -o "$zip" &> "$log" || return 1
	    rm -f "$zip"
	  )
	fi
	
  elif [[ ! -d "$zip_dir" ]]; then
    install_needed=True
    zip="$$.zip"
    curl "${curl_opts[@]}" --progress-bar "$zip_url" -o "$zip" &> "$log" || return 1
	unzip -o "$zip" &> "$log" || return 1
	rm -f "$zip"
  fi

  if [[ "$install_needed" == "True" ]]; then
    source "$common_path/have_ext.sh"
	if [[ -n "$zip_header_subdir" ]]; then
      header_dir="$prefix/include/$zip_header_subdir"
	fi
	(
	  set -euo pipefail
	  cd "$zip_dir"
	  
	  # Add indexes to the static archives
	  if have_ext a; then
	    for archive in *.a; do
		  ranlib "$archive" &> "$log" 
		done
	  fi || true
	  
	  # Ensure we have target directories before copying
	  mkdir -p "$lib_dir" "$bin_dir" "$header_dir" || true
	  
	  # Do a best-effort move of all related files in the prefix targets
	  for ext in dll a so; do
	    have_ext "$ext" && mv *."$ext" "$lib_dir" || true
      done
	  
	  for ext in h hpp; do
	    have_ext "$ext" && mv *."$ext" "$header_dir" || true
	  done
	  
	  for ext in exe bat cmd com; do
        have_ext "$ext" && mv *."$ext" "$bin_dir" || true
	  done	  
    )
    ran=True
  fi
fi
