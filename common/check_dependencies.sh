set -euo pipefail
IFS=$'\n\t'
ran=False

##
#  Check depedendencies
#  --------------------
missing=0
if [[ -n "$dependencies" ]]; then
  for exe in "${dependencies[@]}"; do
    if ! which "$exe" &> /dev/null; then
      missing=$((missing + 1))
      echo "missing: $exe"
    fi
  done
  ran=True
fi

##
#  Inform the user if some are missing
#  -----------------------------------
if [[ "$missing" -gt 0 ]]; then

  if [[ "$platform" == "linux" ]]; then
    echo "One or more dependencies are missing, install them with one of the following:"
    echo " RedHat/CentOS: sudo yum install autoconf automake bzip2 cmake gcc gcc-c++ git libtool make mercurial pkgconfig freetype-devel zlib-devel"
    echo " Ubuntu/Debian: sudo apt install autoconf automake bzip2 cmake gcc g++ git libtool make mercurial pkg-config libfreetype6-dev zlib1g-dev"

  elif [[ "$platform" == "mingw" ]]; then
    echo " Install Git-for-Windows and choose 'Add to your path', then"
    echo " Install MinGW per https://www.dosbox.com/wiki/Building_DOSBox_with_MinGW#Installing_MinGW"
    echo " Don't install SDL and the others because this tool takes care of those."
  else
    echo "Unknown build platform $platform"
  fi

  echo ""
  return 1
fi
