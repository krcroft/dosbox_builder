set -euo pipefail
IFS=$'\n\t'

##
#  Determine architecture
#  ----------------------
arch=""

declare uname=$(uname | awk '{print tolower($0)}' )
declare machine=$(gcc -dumpmachine | awk '{print tolower($0)}')

# 32-bit or 64-bit?
if [[ "$machine" == *"amd64"* ]] || [[ "$machine" == *"x86_64"* ]]; then
  arch=64
else
  arch=32
fi
declare -xr arch
